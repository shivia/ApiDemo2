﻿using DataAccess.GenericRepo;
using Entity.DTO;
using Entity.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiDemo2.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly IAdressRepository _adressRepo;
        private readonly ICustomerRepository _customerRepo;
        private readonly IDepartmentRepository _departmentRepo;
        private readonly IGenericRepository<BaseEntity> _genericRepo;

        public CustomerController(IAdressRepository adressRepo, ICustomerRepository customerRepo, IDepartmentRepository departmentRepo, IGenericRepository<BaseEntity> genericRepo)
        {
            _adressRepo = adressRepo;
            _customerRepo = customerRepo;
            _departmentRepo = departmentRepo;
            _genericRepo = genericRepo;
        }

        public async Task<IActionResult> Index()
        {

            CustomerDTO customerDto = new CustomerDTO();
            customerDto.customer = new Customer { Name = "Rabia Akdemir", Email = "ra@gmail.com", PhoneNumber = 468, Department = new Department { Name = "HR" } };
            customerDto.department = customerDto.customer.Department;
            customerDto.adresses = new List<Adress>
            {
                new Adress { Customer = customerDto.customer, City = "Adana", Country = "Barajyolu", Name = "Home", Street = "63" }
            };

            await _customerRepo.Create(customerDto.customer);

            return Ok();
        }

    }
}
