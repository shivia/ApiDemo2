﻿
using DataAccess.DatabaseContext;
using Entity.Entities;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.GenericRepo
{
    /*
 * TEntity modellere(entitylere) veritabanındaki tablolara denk gelmektedir. 
 * _dbContext.Set<TEntity> metodunu DbSet<> objectlerine erişmek için kullanıyoruz
  */

    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : BaseEntity
    {
        private readonly CustomerDbContext _dbcontext;
        public GenericRepository(CustomerDbContext dbContext)
        {
            _dbcontext = dbContext;
        }
        /*
         *uygulamaların performansını optimize etmemize yardımcı olmak için AsNoTracking() metodunu kullanıyoruz.
         *sadece read işlemlerinde kullanırız.
         */
        public IQueryable<TEntity> GetAll()
        {
            return _dbcontext.Set<TEntity>().AsNoTracking();
        } 

        public async Task<TEntity> GetById(int id)
        {
            return await _dbcontext.Set<TEntity>()
                .AsNoTracking().FirstOrDefaultAsync(e => e.Id == id);
        }

        public async Task Create(TEntity entity)
        {
            await _dbcontext.Set<TEntity>().AddAsync(entity);
            await _dbcontext.SaveChangesAsync();
        }

        public async Task Update(int id, TEntity entity)
        {
            _dbcontext.Set<TEntity>().Update(entity);
            await _dbcontext.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            var entity = await GetById(id);
            _dbcontext.Set<TEntity>().Remove(entity);
            await _dbcontext.SaveChangesAsync();
        }

    }
}
