﻿using DataAccess.DatabaseContext;
using Entity.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.GenericRepo
{
    public class AdressRepository : GenericRepository<Adress>, IAdressRepository
    {
        public AdressRepository(CustomerDbContext dbContext) : base(dbContext)
        {

        }
    }
}
