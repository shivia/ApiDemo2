﻿using DataAccess.DatabaseContext;
using Entity.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.GenericRepo
{
    public class CustomerRepository : GenericRepository<Customer>, ICustomerRepository 
    {
        public CustomerRepository(CustomerDbContext dbContext): base(dbContext)
        {

        }

        //public async Task<string> GetFirstCustomerAdress()
        //{
        //    return await GetAll().Select(x => x.Name).FirstOrDefaultAsync();
        //}
    }
}
