﻿using Entity.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.GenericRepo
{
    //IBookRepo interface IGenericRepository interfacden bütün CRUD işlemlerini Customer nesnesine alıyor

    public interface ICustomerRepository : IGenericRepository<Customer>
    {
        //CRUD işlemleri dışında CustomerRepoya ayrıca işlem yapmak istiyorsak bu arayüzde belirtebiliriz
        //Task<string> GetFirstCustomerAdress();
    }
}
