﻿using Entity.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DataAccess.Common.Enum;

namespace DataAccess.DatabaseContext
{
    public class CustomerDbContext : DbContext
    {
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<Adress> Adress { get; set; }
        public virtual DbSet<Department> Department { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {


            if (!optionsBuilder.IsConfigured)
            {
                var database = Common.ConfigurationManager.Configuration.GetSection("DefaultDatabase").Get<DatabasePreference>();

                if (!string.IsNullOrEmpty(database.ToString()))
                    optionsBuilder.UseSqlServer(Common.ConfigurationManager.Configuration.GetConnectionString(database.ToString()));

            }
        }
    }
}
