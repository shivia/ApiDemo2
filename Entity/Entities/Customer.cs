﻿using System;
using System.Collections.Generic;

using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Entities
{
    [Table("Customer")]
    public class Customer : BaseEntity
    {
        public string Email { get; set; }
        public int PhoneNumber { get; set; }
        public int DepRefId { get; set; }
        [ForeignKey("DepRefId")]
        public Department Department { get; set; }
    }
}
