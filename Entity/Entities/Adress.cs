﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Entities
{
    [Table("Adress")]
    public class Adress : BaseEntity
    {
        public string Country { get; set; }
        public string City { get; set; }

        public string Street { get; set; }

        public int CustomerRefId { get; set; }

        [ForeignKey("CustomerRefId")]
        public Customer Customer { get; set; }
    }
}
