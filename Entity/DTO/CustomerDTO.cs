﻿using Entity.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.DTO
{
    public class CustomerDTO
    {
        public CustomerDTO()
        {
            
            this.adresses = new List<Adress>();
            
        }

        public Customer customer { get; set; }
        public List<Adress> adresses { get; set; }
        public Department department { get; set; }


    }
}
